# PostGraphTG #



### What is PostGraphTG? ###

**PostGraphTG** is an integration of the relational database **PostgreSQL** 
with **tgCloud**, **pyTigerGraph**, and **Graph App Kit** (**Graphistry** + **Streamlit**).